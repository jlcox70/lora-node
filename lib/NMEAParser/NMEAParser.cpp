#include "Arduino.h"
#include "NMEAParser.h"
#include <string.h>

NMEAParser::NMEAParser() {}

bool NMEAParser::validateSentence(const char* sentence) {
    // // Create a buffer to store the sentence and split it into fields
    // char buffer[strlen(sentence) + 1];
    // strcpy(buffer, sentence);

    // // Initialize variables for parsing
    // char* token = strtok(buffer, ",");
    // int fieldCount = getFieldCount(sentence);

    // // Determine the expected number of fields based on the sentence type
    // const char* sentenceType = token;
    // int expectedFields[] = {14, 12, -1, 10, 6, 8}; // -1 indicates variable fields (GPGSV)

    // int expectedCount = expectedFields[0]; // Default for unknown sentence types
    // if (strcmp(sentenceType, "$GPGGA") == 0) expectedCount = expectedFields[0];
    // else if (strcmp(sentenceType, "$GPGSA") == 0) expectedCount = expectedFields[1];
    // else if (strcmp(sentenceType, "$GPGSV") == 0) return true; // Variable number of fields
    // else if (strcmp(sentenceType, "$GPRMC") == 0) expectedCount = expectedFields[3];
    // // else if (strcmp(sentenceType, "$GPGLL") == 0) expectedCount = expectedFields[4];
    // else if (strcmp(sentenceType, "$GPGLL") == 0) return true;
    // else if (strcmp(sentenceType, "$GPVTG") == 0) expectedCount = expectedFields[5];

    // if (fieldCount != expectedCount){
    // Serial.print(sentenceType);
    // Serial.print(" Expect: " );
    // Serial.print(expectedCount);
    // Serial.print(" Actual: ");
    // Serial.println(fieldCount);
    // }
    // return (fieldCount == expectedCount);
    // Verify the checksum
    if (!verifyChecksum(sentence)) {
        return false;
    }

    // Additional validation logic if needed
    
    return true;
}

int NMEAParser::getFieldCount(const char* sentence) {
    // Count the number of fields in the sentence
    int count = 0;
    char* token = strtok((char*)sentence, ",");
    while (token != NULL) {
        count++;
        token = strtok(NULL, ",");
    }
    return count;
}


bool NMEAParser::verifyChecksum(const char* sentence) {
    // Find the position of the '*' character
    const char* asteriskPos = strchr(sentence, '*');

    // Check if there's an asterisk and at least two characters after it
    if (asteriskPos == NULL || strlen(asteriskPos) < 3) {
        return false;
    }

    // Extract the checksum part from the sentence
    char checksumStr[3];
    strncpy(checksumStr, asteriskPos + 1, 2);
    checksumStr[2] = '\0';

    // Convert the checksum string to an integer
    int expectedChecksum = strtol(checksumStr, NULL, 16);

    // Calculate the checksum for the sentence (excluding the '$' character)
    int calculatedChecksum = 0;
    for (int i = 1; i < asteriskPos - sentence; i++) {
        calculatedChecksum ^= sentence[i];
    }

    return (calculatedChecksum == expectedChecksum);
}