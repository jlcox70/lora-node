#ifndef NMEA_PARSER_H
#define NMEA_PARSER_H

#include "Arduino.h"

class NMEAParser;
class NMEAParser {
    public:
        NMEAParser();
        bool validateSentence(const char* sentence);
    private:
        int getFieldCount(const char* sentence);
        bool verifyChecksum(const char* sentence);
};

#endif // NMEA_PARSER_H
