#ifndef _DEFINITIONS_H__
#define _DEFINITIONS_H__

//GPS is connected to Serial1, we can do a quick rename to remind us
#define gps Serial1

const short power_led = 6;
const short sd_led = 5;
const short gps_led = 3;
// const short charge_pin = A0;

//simple macros
#define SECONDS(x) (x * 1000)
#define MINUTES(x) (SECONDS(60) * x)
#define HOURS(x) (MINUTES(60) * x)


typedef struct
{
  unsigned long power_led_timer = 0;
  unsigned long gps_lasttime = 0;
} ledTimers;

int ignorePackets = 5;
int packetCount ;

// typedef uint8_t byte;
byte msgCount = 0;            // count of outgoing messages
byte localAddress = 0xF0;     // address of this device
byte destination = 0xFF;      // destination to send to

int lora_SF = 9 ;
long lora_BW = 125E3;
long lora_CR = 5;
int lora_TX_P = 20;
long lora_frequency = 915E6;
int lora_GAIN = 6;
int lora_TX_PA = 1;
const int csPin = 10;   // LoRa radio chip select
const int resetPin = 9; // LoRa radio reset
const int irqPin = 2;   // change for your board; must be a hardware interrupt pin


int led = 13;
bool ledState = LOW;



unsigned long gpsInterval = 10000; // Set the interval to 10 seconds
unsigned long lastGpsSendTime = 0;
unsigned long lastReceiveTime = 0; // New variable to track the last received time
const unsigned long rxTimeout = 10000; // Timeout for RX mode (10 seconds)


#endif