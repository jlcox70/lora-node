#include <Arduino.h>
/*
  LoRa Simple Gateway/Node Exemple

  This code uses InvertIQ function to create a simple Gateway/Node logic.

  Gateway - Sends messages with enableInvertIQ()
          - Receives messages with disableInvertIQ()

  Node    - Sends messages with disableInvertIQ()
          - Receives messages with enableInvertIQ()

  With this arrangement a Gateway never receive messages from another Gateway
  and a Node never receive message from another Node.
  Only Gateway to Node and vice versa.

  This code receives messages and sends a message every second.

  InvertIQ function basically invert the LoRa I and Q signals.

  See the Semtech datasheet, http://www.semtech.com/images/datasheet/sx1276.pdf
  for more on InvertIQ register 0x33.

  created 05 August 2018
  by Luiz H. Cassettari
*/

#include <SPI.h>              // include libraries
#include <LoRa.h>
#include "definitions.h"
#include <SoftwareSerial.h>
#include "NMEAParser.h"
#include <Adafruit_SleepyDog.h>

//display
#include <GxEPD2_3C.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include "display.h"
#include "bitmaps/Bitmaps200x200.h" 

SoftwareSerial Serial1(4,5);



NMEAParser nmeaParser;

String message;
String sendMessage;
String gpsMessage;

void ledToggle()
{
  ledState = !ledState;
  digitalWrite(led, ledState);
}

void LoRa_rxMode(){
  LoRa.enableInvertIQ();                // active invert I and Q signals
  LoRa.receive();                       // set receive mode
}

void LoRa_txMode(){
  LoRa.idle();                          // set standby mode
  LoRa.disableInvertIQ();               // normal mode
}

void LoRa_sendMessage(String sendMessage) {
  
  // Serial.print("sending message : ");
  // Serial.println(message);
  LoRa_txMode();                        // set tx mode
  LoRa.beginPacket();                   // start packet
  LoRa.print(sendMessage);                  // add payload
  LoRa.endPacket();                 // finish packet and send it
  LoRa_rxMode();
}

void onReceive(int packetSize) {
    Watchdog.reset();

  String message = "";

  while (LoRa.available()) {
    message += (char)LoRa.read();
  }

  Serial.print("Node Receive: ");
  Serial.println(message);
    ledToggle();

  lastReceiveTime = millis();
}

void onTxDone() {
  Serial.print("");
  LoRa_rxMode();
}

boolean runEvery(unsigned long interval)
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;
    return true;
  }
  return false;
}


String screeMsg (){
  String msg = "GPS location\n";
  // msg += "1\n";
  // msg += "2\n";
  // msg += "3\n";
  // msg += "4\n";
  // msg += "5\n";
  // msg += "6\n";
  return msg;
}

void dispSNR(String dispMsg)
{
  //Serial.println("helloWorld");
  display.setRotation(1);
  display.setFont(&FreeMonoBold9pt7b);
  if (display.epd2.WIDTH < 104) display.setFont(0);
  display.setTextColor(GxEPD_BLACK);
  int16_t tbx, tby; uint16_t tbw, tbh;
  display.getTextBounds(dispMsg.c_str(), 0, 0, &tbx, &tby, &tbw, &tbh);
  // center bounding box by transposition of origin:
  uint16_t x = ((display.width()- tbw) ) - tbx;
  uint16_t y = ((display.height() - tbh) ) - tby;
  display.setFullWindow();
  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(x, y);
    display.print(dispMsg.c_str());
  }
  while (display.nextPage());
  //Serial.println("helloWorld done");
}

void setup() {
  Serial.begin(115200);                   // initialize serial
  // while (!Serial);
  gps.begin(9600);
  // display.init(115200, true, 2, false);
  // dispSNR(screeMsg());
  // display.end( );





  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  gps.setTimeout(1100); //if we don't receive anything in 1.1 seconds, timeout
  
  // setup watchdog
    int countdownMS = Watchdog.enable(30000);
    
  // setupPins();
  LoRa.setPins(csPin, resetPin, irqPin);

  if (!LoRa.begin(lora_frequency)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
  Serial.println();
  Serial.println("LoRa Simple Node");
  Serial.println("Only receive messages from gateways");
  Serial.println("Tx: invertIQ disable");
  Serial.println("Rx: invertIQ enable");
  Serial.println();
  LoRa.setSyncWord(0xDB);
  // LoRa.setSpreadingFactor(lora_SF); // ranges from 6-12,default 7 see API docs
  // LoRa.setCodingRate4(lora_CR);
  // LoRa.setSignalBandwidth(lora_BW);
  // LoRa.enableCrc();
  // LoRa.setGain(lora_GAIN);
  // LoRa.setTxPower(lora_TX_P, lora_TX_PA);
  
  LoRa.onReceive(onReceive);
  LoRa.onTxDone(onTxDone);
  LoRa_rxMode();

}

void loop() {
  Watchdog.reset();

// LoRa_rxMode();
  if (runEvery(10000)) { // repeat every 1000 millis
    String sendMessage ="";
    String  coords ="";
    String utc ="";
    String date ="";
    sendMessage = "HeLoRa World! ";
    sendMessage += "I'm a Node! ";
    sendMessage += millis();
    sendMessage += "\n";
    sendMessage += "GPS Data: " + coords + " UTC: " + utc + " Date: " + date + "\n";
 
    Serial.println("sending message:");
    Serial.println(sendMessage);

    LoRa_sendMessage(sendMessage);
  }
}